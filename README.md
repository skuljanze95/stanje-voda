# stanje_voda

Android Flutter app that displays water temperatures scraped from Link with the help of python script running on AWS EC2 instance.

Temperatures are saved on Firebase and are displayed in real-time

(App is not published or finished)

Technologies:
- Flutter
- Python
- Firebase
- AWS EC2

![Image description](https://i.ibb.co/fG2Q5mM/Group-9.png)