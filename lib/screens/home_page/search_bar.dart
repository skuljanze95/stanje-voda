import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class SearchBar extends StatefulWidget {
  String search_bar;
  Function(String) callback;

  SearchBar(this.search_bar, this.callback);

  @override
  _SearchBarState createState() => _SearchBarState();
}

class _SearchBarState extends State<SearchBar> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: Container(
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: new BorderRadius.all(const Radius.circular(10.0)),
        ),
        margin: EdgeInsets.fromLTRB(25, 0, 25, 0),
        padding: EdgeInsets.fromLTRB(25, 0, 25, 0),
        child: TextField(
          decoration: InputDecoration(
            border: InputBorder.none,
            contentPadding: EdgeInsets.fromLTRB(0, 15.0, 0, 15.0),
            icon: Icon(
              Icons.search,
              color: const Color(0xFF00B4DB),
            ),
            hintText: "Poišči reko...",
            hintStyle: TextStyle(fontFamily: 'Poppins'),
          ),
          onChanged: (text) {
            text.isEmpty
                ? widget.callback(text)
                : widget
                    .callback('${text[0].toUpperCase()}${text.substring(1)}');
          },
        ),
      ),
    );
  }
}
