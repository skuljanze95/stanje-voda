import 'package:flutter/material.dart';
import 'firestore_list.dart';

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
          decoration: BoxDecoration(
            gradient: LinearGradient(
              colors: [const Color(0xFF0083B0), const Color(0xFF00B4DB)],
            ),
          ),
          child: FirestoreList()),
    );
  }
}
