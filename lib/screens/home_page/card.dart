import 'package:flutter/material.dart';

class Place_Card extends StatelessWidget {
  final String _reka;
  final String _mesto;
  final String _temperatura;
  final String _slika;

  Place_Card(
    this._reka,
    this._mesto,
    this._temperatura,
    this._slika,
  );

  @override
  Widget build(BuildContext context) {
    // Material is a conceptual piece of paper on which the UI appears.
    return Container(
      margin: EdgeInsets.fromLTRB(6.0, 8.0, 6.0, 8.0),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: new BorderRadius.all(const Radius.circular(10.0)),
      ),
      child: ListTileTheme(
        contentPadding: EdgeInsets.fromLTRB(0, 0, 15.0, 0),
        child: ExpansionTile(
          leading: Container(
            padding: EdgeInsets.all(11),
            width: 95,
            decoration: BoxDecoration(
              gradient: LinearGradient(
                colors: [const Color(0xFF0083B0), const Color(0xFF00B4DB)],
              ),
              borderRadius: new BorderRadius.only(
                  topLeft: const Radius.circular(10.0),
                  bottomLeft: const Radius.circular(10.0),
                  bottomRight: const Radius.circular(20.0),
                  topRight: const Radius.circular(60.0)),
            ),
            child: Text(
              _temperatura,
              style: TextStyle(
                  color: Colors.white,
                  fontSize: 23,
                  fontFamily: 'Poppins',
                  letterSpacing: 0.3),
            ),
          ),
          title: Container(
            child: Column(children: [
              Text(
                "$_reka                                           ",
                style: TextStyle(
                  color: Colors.black,
                  fontSize: 17,
                  fontFamily: 'Poppins',
                ),
                textAlign: TextAlign.left,
              ),
              Text(
                "$_mesto                                          ",
                style: TextStyle(
                  color: Colors.black,
                  fontSize: 13,
                  fontFamily: 'Poppins',
                  letterSpacing: 0.3,
                ),
                textAlign: TextAlign.left,
              ),
            ]),
          ),
          children: <Widget>[
            Container(
              padding: EdgeInsets.all(15),
              child: ClipRRect(
                borderRadius: BorderRadius.all(Radius.circular(10.0)),
                child: Container(
                    child: Image.network(
                  _slika,
                )),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
