import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/rendering.dart';
import 'package:stanje_voda/screens/home_page/search_bar.dart';
import 'package:stanje_voda/screens/home_page/card.dart';

class FirestoreList extends StatefulWidget {
  @override
  _FirestoreListState createState() => _FirestoreListState();
}

class _FirestoreListState extends State<FirestoreList> {
  String search_bar = "";

  callback(search_string) {
    setState(() {
      search_bar = search_string;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.fromLTRB(0, 100.0, 0, 0),
      child: Column(
        children: <Widget>[
          SizedBox(
            width: double.infinity,
            child: Padding(
              padding: const EdgeInsets.fromLTRB(25.0, 0, 0, 20),
              child: Text("Stanje Voda",
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 35,
                  ),
                  textAlign: TextAlign.left),
            ),
          ),
          SearchBar(search_bar, callback),
          Expanded(
            child: Container(
              margin: EdgeInsets.fromLTRB(0, 20.0, 0, 0),
              padding: EdgeInsets.fromLTRB(15.0, 10.0, 15.0, 0),
              decoration: BoxDecoration(
                color: const Color(0xFFEBEBEB),
                borderRadius: new BorderRadius.only(
                    topLeft: const Radius.circular(50.0),
                    topRight: const Radius.circular(50.0)),
              ),
              child: StreamBuilder(
                  stream: Firestore.instance
                      .collection("sampleData")
                      .where("Reka", isGreaterThanOrEqualTo: search_bar)
                      .where("Reka", isLessThanOrEqualTo: search_bar + "x")
                      .snapshots(),
                  builder: (context, snapshot) {
                    if (!snapshot.hasData) {
                      return Text("Nalagam...");
                    } else {
                      return ListView.builder(
                          itemCount: snapshot.data.documents.length,
                          itemBuilder: (context, index) {
                            DocumentSnapshot docsSnap =
                                snapshot.data.documents[index];
                            return GestureDetector(
                              onTap: () {
                                print("#");
                              },
                              child: Place_Card(
                                "${docsSnap["Reka"]}",
                                "${docsSnap["Mesto"]}",
                                "${docsSnap["Temperatura"]}°C",
                                "https://cdn.images.express.co.uk/img/dynamic/151/590x/Black-hole-picture-please-time-date-first-image-black-hole-event-horizon-telescope-1112295.jpg?r=1554889792953",
                              ),
                            );
                          });
                    }
                  }),
            ),
          ),
        ],
      ),
    );
  }
}
