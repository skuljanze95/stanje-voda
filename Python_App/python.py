
import firebase_admin
from firebase_admin import credentials, firestore

from urllib.request import urlopen as uReq
from bs4 import BeautifulSoup as soup

from twisted.internet import task, reactor

# Fetch the service account key JSON file contents
cred = credentials.Certificate(
    'stanje-voda-18e46-firebase-adminsdk-4f3ae-d885fe0104.json')
# Url for river data
my_url = "https://www.arso.gov.si/vode/podatki/stanje_voda_samodejne.html"

# Initialize the app with a service account, granting admin privileges
default_app = firebase_admin.initialize_app(cred)
db = firestore.client()

def my_function():

    # Web scraping

    uClient = uReq(my_url)
    page_html = uClient.read()

    # Web parsing

    page_soup = soup(page_html, "html.parser")
    table = page_soup.find(text="Krka")
    data = []
    table_p = table.parent.parent.parent
    rows = table_p.findAll("tr")
    for row in rows:
        cells = row.findAll('td')
        data.append(cells)

    data.remove(data[0])
    data.remove(data[0])

    i = 0
    for row in data:
        
        print(data[i][1].string + " " + data[i][5].string + " °C")
        doc_ref = db.collection(u'sampleData').document(data[i][1].string).set({
             u'Reka': data[i][0].string,
             u'Mesto': data[i][1].string,
             u'Temperatura': data[i][5].string,
            })
        
        i+=1
    

        

timeout = 3600.0

def doWork():
    my_function()
    print("test")
    pass

l = task.LoopingCall(doWork)
l.start(timeout) 

reactor.run()
























